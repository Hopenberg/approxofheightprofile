Approximation of height profiles using interpolation methods.

Implemented interpolation methods are:
- interpolation using Lagrange functions
- interpolation using splines

Data to analize is retrieved from csv files.