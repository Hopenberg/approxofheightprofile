import csv
import sys
import numpy as np
import matplotlib.pyplot as plt
from CsvDataParser import CsvDataParser
from matrix.Matrix import Matrix
from num_methods.ForwardSubstitution import ForwardSubstitution
from num_methods.BackwardSubstitution import BackwardSubstitution
from scipy.linalg import solve_triangular


