import numpy as np

class LagrangeInterpolation:
    __numberOfLearningPoints = 0
    __learningPoints = None
    __dataToInterpolate = None
    __resultOfInterpolation = None
    __currentXValue = 0
    __resultYValue = 0

    def __init__(self, dataToInterpolate, numberOfPoints):
        self.__dataToInterpolate = np.copy(dataToInterpolate)
        self.__numberOfLearningPoints = numberOfPoints

    def compute(self):
        self.__setResultOfInterpolationXVals()
        self.__retrieveLearningPoints()
        for currentXIndex in range(0, np.size(self.__resultOfInterpolation, 0)):
            self.__currentXValue = self.__resultOfInterpolation[currentXIndex][0]
            for i in range(0, self.__numberOfLearningPoints):
                self.__resultYValue += self.__learningPoints[i][1]*self.__lagrangeFuncIndexOf(i)
            self.__resultOfInterpolation[currentXIndex][1] = self.__resultYValue
            self.__resultYValue = 0

    def __setResultOfInterpolationXVals(self):
        self.__resultOfInterpolation = np.zeros((np.size(self.__dataToInterpolate, 0), 2))
        self.__resultOfInterpolation[:, 0] = np.copy(self.__dataToInterpolate[:, 0])

    def __retrieveLearningPoints(self):
        self.__learningPoints = np.zeros((self.__numberOfLearningPoints, 2))
        dataNumberOfRows = np.size(self.__dataToInterpolate, 0)
        dataIndex = 0

        pointsStep = int(dataNumberOfRows / (self.__numberOfLearningPoints - 1))
        for i in range(0, self.__numberOfLearningPoints):
            if dataIndex >= dataNumberOfRows:
                dataIndex = dataNumberOfRows - 1
            self.__learningPoints[i] = self.__dataToInterpolate[dataIndex]
            dataIndex += pointsStep

    def __lagrangeFuncIndexOf(self, index):
        i = index
        result = 1.0
        for j in range(0, self.__numberOfLearningPoints):
            if j == i:
                pass
            else:
                    result *= (self.__currentXValue - self.__learningPoints[j][0]) /\
                              (self.__learningPoints[i][0] - self.__learningPoints[j][0])

        return result

    def getResultOfInterpolation(self):
        return self.__resultOfInterpolation
