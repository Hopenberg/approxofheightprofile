import time

import numpy as np
from matrix.Matrix import Matrix


class BackwardSubstitution:
    def __init__(self, U, b):
        self.U = U
        self.b = b
        self.time = 0
        self.x = []

    def pivote(self, mat, row, column):
        iMax = 0
        matrix = np.array(mat)
        for i in range(0, self.U.cols):
            if abs(mat[i][column]) >= abs(mat[iMax][column]):
                iMax = i
        temp = np.copy(matrix[row, :])
        matrix[row, :] = matrix[iMax, :]
        matrix[iMax, :] = temp
        right = self.b.matrix

        temp = right[row][0]
        right[row][0] = right[iMax][0]
        right[iMax][0] = temp
        return matrix, right

    def compute(self, diag=None, horizontalRange=None):
        if diag:
            lu = self.U.add(diag)
        else:
            lu = self.U

        templu = lu.matrix  # A = L + D
        result = np.zeros((lu.rows, 1))
        right = self.b.matrix

        start = time.time()
        j = 0
        for i in range(lu.rows - 1, j - 1, -1):
            if i == lu.rows - 1:  # this if to prevent index out of bound
                if templu[i][i] == 0:
                    pass # templu, right = self.pivote(templu, i, i)
                else:
                    result[i][j] = right[i][0] / templu[i][i]
            else:
                tempsum = 0
                if horizontalRange:
                    for k in range(i + 1, i + horizontalRange):
                        if k < lu.cols:
                            tempsum += templu[i][k] * result[k][j]
                else:
                    for k in range(i + 1, lu.cols):
                        tempsum += templu[i][k] * result[k][j]
                if templu[i][i] == 0:
                    pass  # templu, right = self.pivote(templu, i, i)
                else:
                    result[i][j] = (right[i][0] - tempsum) / templu[i][i]
        end = time.time()
        self.time = end - start
        self.x = result
        return Matrix(result)


