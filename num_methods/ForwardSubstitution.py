import time

import numpy as np
from matrix.Matrix import Matrix


class ForwardSubstitution:
    def __init__(self, L, b):
        self.L = L
        self.b = b
        self.time = 0
        self.x = []

    def compute(self, diag=None, horizontalRange=None):
        if diag:
            lu = self.L.add(diag)
        else:
            lu = self.L

        templu = lu.matrix  # A = L + D
        result = np.zeros((lu.rows, 1))
        right = self.b.matrix

        start = time.time()
        j = 0
        for i in range(j, lu.rows):
            if i == j:  # this if to prevent index out of bound
                if templu[i][i] == 0:
                    pass
                else:
                    result[i][j] = right[i][0] / templu[i][i]
            else:
                tempsum = 0
                if horizontalRange:
                    for k in range(i - horizontalRange, i):
                        if k >= 0:
                            tempsum += templu[i][k] * result[k][j]
                else:
                    for k in range(j, i):
                        tempsum += templu[i][k] * result[k][j]
                if templu[i][i] == 0:
                    pass
                else:
                    result[i][j] = (right[i][0] - tempsum) / templu[i][i]
        end = time.time()
        self.time = end - start
        self.x = result
        return Matrix(result)
