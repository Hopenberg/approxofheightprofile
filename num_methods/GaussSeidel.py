from matrix.Matrix import Matrix
from matrix.Inversion import Inversion
import time
import numpy as np


class GaussSeifel:
    def __init__(self, A, b):
        self.A = A
        self.b = b
        self.iter = 0
        self.resVector = []
        self.time = 0
        self.r = []

    def compute(self, stop=10**-6, maxiter=100000):
        r = Matrix(np.ones((self.b.rows, 1)))  # inital values are ones
        iter = 0
        invDL = Inversion().invLowerTriangle(self.A.tril(), self.A.diag())
        U = self.A.triu()
        invDLb = invDL.multiply(self.b)

        start = time.time()
        while self.iter < maxiter:
            norm = self.A.multiply(r).sub(self.b).vecNorm()

            if norm <= stop:
                break
            self.iter += 1
            self.resVector.append(norm)
            Ur = U.multiply(r)
            r = invDL.multiply(Ur).multiply(-1).add(invDLb)  # just different order
            # r = invDL.multiply(U).multiply(r).multiply(-1).add(invDLb)  # invDL*(U)*r*(-1) + invDL*b
        self.r = r
        end = time.time()
        self.time = end-start
