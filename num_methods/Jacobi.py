import time

from matrix.Matrix import Matrix
from matrix.Inversion import Inversion
import numpy as np


class Jacobi:
    def __init__(self, A, b):
        self.A = A
        self.b = b
        self.iter = 0
        self.resVector = []
        self.time = 0
        self.r = []

    def compute(self, stop=10**-6, maxiter=100000):
        r = Matrix(np.ones((self.b.rows, 1)))  # inital values are ones
        invD = Inversion().invDiag(self.A.diag())
        LU = self.A.triu().add(self.A.tril())
        invDb = invD.multiply(self.b)

        start = time.time()
        while self.iter < maxiter:
            norm = self.A.multiply(r).sub(self.b).vecNorm()

            if norm <= stop:
                break
            self.iter += 1
            self.resVector.append(norm)
            LUr = LU.multiply(r)
            r = invD.multiply(LUr).multiply(-1).add(invDb)

        self.r = r
        end = time.time()
        self.time = end-start
