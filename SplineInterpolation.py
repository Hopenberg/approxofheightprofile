import numpy as np
from matrix.Matrix import Matrix
from num_methods.ForwardSubstitution import ForwardSubstitution
from num_methods.BackwardSubstitution import BackwardSubstitution


class SplineInterpolation:
    __X_INDEX = 0
    __Y_INDEX = 1
    __dataToInterpolate = None
    __numberOfLearningNodes = 0
    __learningNodes = None
    __resultOfInterpolation = None
    __xResultVector = None
    __matrixA = None
    __bVector = None
    __numberOfSubsections = 0

    def __init__(self, dataToInterpolate, numberOfLearningPoints):
        self.__dataToInterpolate = np.copy(dataToInterpolate)
        self.__numberOfLearningNodes = numberOfLearningPoints
        self.__numberOfSubsections = self.__numberOfLearningNodes - 1
        self.__setResultOfInterpolationXValues()
        self.__retrieveLearningNodes()

    def compute(self):
        self.__setValuesForLinearEquation()
        self.__computeLinearEquationUsingLUDecomp()
        self.__fillResultContainerWithApproximatedValues()

    def __setResultOfInterpolationXValues(self):
        self.__resultOfInterpolation = np.zeros((np.size(self.__dataToInterpolate, 0), 2))
        self.__resultOfInterpolation[:, 0] = np.copy(self.__dataToInterpolate[:, 0])

    def __retrieveLearningNodes(self):
        self.__learningNodes = np.zeros((self.__numberOfLearningNodes, 2))
        dataNumberOfRows = np.size(self.__dataToInterpolate, 0)
        dataIndex = 0

        pointsStep = int(dataNumberOfRows / (self.__numberOfLearningNodes - 1))
        for i in range(0, self.__numberOfLearningNodes):
            if dataIndex >= dataNumberOfRows:
                dataIndex = dataNumberOfRows - 1
            self.__learningNodes[i] = self.__dataToInterpolate[dataIndex]
            dataIndex += pointsStep

    def __setValuesForLinearEquation(self):
        self.__createbVector()
        self.__createAMatrix()

    def __createAMatrix(self):
        numberOfDerivatives = self.__numberOfSubsections - 1
        self.__matrixA = np.zeros((4*self.__numberOfSubsections, 4*self.__numberOfSubsections))

        self.__fillPolynomialPartOfMatrix()
        self.__fillDerivativesPartOfMatrix(numberOfDerivatives)
        self.__fillLastPartOfMatrix()

    def __fillPolynomialPartOfMatrix(self):
        currentMatrixRow = 0
        for i in range(self.__numberOfSubsections):
            currentPolynomialCoefficients = 4 * i
            currentDifference = self.__learningNodes[i + 1][self.__X_INDEX] - self.__learningNodes[i][self.__X_INDEX]
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients] = 1
            currentMatrixRow += 1
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients] = 1
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients + 1] = currentDifference ** 1
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients + 2] = currentDifference ** 2
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients + 3] = currentDifference ** 3
            currentMatrixRow += 1

    def __fillDerivativesPartOfMatrix(self, numberOfDerivatives):
        currentMatrixRow = 2 * self.__numberOfSubsections
        for i in range(numberOfDerivatives):
            currentPolynomialCoefficients = 4 * i
            currentDifference = self.__learningNodes[i + 1][self.__X_INDEX] - self.__learningNodes[i][self.__X_INDEX]
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients + 1] = 1
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients + 2] = 2 * currentDifference
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients + 3] = 3 * (currentDifference ** 2)

            self.__matrixA[currentMatrixRow + numberOfDerivatives][currentPolynomialCoefficients + 2] = 2
            self.__matrixA[currentMatrixRow + numberOfDerivatives][currentPolynomialCoefficients + 3] = 6 * currentDifference

            currentPolynomialCoefficients = 4 * (i + 1)
            self.__matrixA[currentMatrixRow][currentPolynomialCoefficients + 1] = -1
            self.__matrixA[currentMatrixRow + numberOfDerivatives][currentPolynomialCoefficients + 2] = -2
            currentMatrixRow += 1

    def __fillLastPartOfMatrix(self):
        currentMatrixRow = 4*self.__numberOfSubsections - 2
        self.__matrixA[currentMatrixRow][2] = 1
        currentMatrixRow += 1
        self.__matrixA[currentMatrixRow][4 * (self.__numberOfSubsections - 1) + 2] = 2
        lastDifference = \
            self.__learningNodes[self.__numberOfLearningNodes - 1][self.__X_INDEX] - \
            self.__learningNodes[self.__numberOfLearningNodes - 2][self.__X_INDEX]
        self.__matrixA[currentMatrixRow][4 * (self.__numberOfSubsections - 1) + 3] = 6 * lastDifference

    def __createbVector(self):
        self.__bVector = np.zeros((4*self.__numberOfSubsections, 1))
        self.__fillBoundaryValuesOfBVector()
        self.__fillMiddleValuesOfBVector()

    def __fillBoundaryValuesOfBVector(self):
        self.__bVector[0] = self.__learningNodes[0][self.__Y_INDEX]
        self.__bVector[(2 * self.__numberOfSubsections) - 1] = \
            self.__learningNodes[self.__numberOfLearningNodes - 1][self.__Y_INDEX]

    def __fillMiddleValuesOfBVector(self):
        learningNodesIndex = 1
        for i in range(1, (2 * self.__numberOfSubsections) - 1, 2):
            self.__bVector[i] = self.__learningNodes[learningNodesIndex][self.__Y_INDEX]
            self.__bVector[i + 1] = self.__learningNodes[learningNodesIndex][self.__Y_INDEX]
            learningNodesIndex += 1

    def __computeLinearEquationUsingLUDecomp(self):
        A = Matrix(self.__matrixA)
        L, U, P = A.LUFactorization()  # LUx = b
        fs = ForwardSubstitution(L, P.multiply(Matrix(self.__bVector)))  # Ly = P*b
        y = fs.compute()
        bs = BackwardSubstitution(U, y)  # Ux = y
        self.__xResultVector = (bs.compute()).getMatrix()

    def __fillResultContainerWithApproximatedValues(self):
        resultIndex = 0
        for i in range(self.__numberOfSubsections):
            currentVector = Matrix(self.__xResultVector[4 * i:(4 * i) + 4])
            while self.__resultOfInterpolation[resultIndex][self.__X_INDEX] <= self.__learningNodes[i + 1][self.__X_INDEX]:
                xValue = self.__resultOfInterpolation[resultIndex][self.__X_INDEX]
                currentDifference = xValue - self.__learningNodes[i][self.__X_INDEX]
                leftVec = Matrix(np.array([[1, currentDifference, currentDifference ** 2, currentDifference ** 3]]))
                result = leftVec.multiply(currentVector)
                self.__resultOfInterpolation[resultIndex][self.__Y_INDEX] = result.getMatrix()[0]
                resultIndex += 1
                if self.__dataToInterpolate[resultIndex][self.__X_INDEX] == self.__learningNodes[self.__numberOfLearningNodes- 1][self.__X_INDEX]:
                    self.__deleteRestOfData(resultIndex)
                    break

    def __deleteRestOfData(self, resultIndex):
        size = np.size(self.__resultOfInterpolation, 0)
        range = np.arange(size - resultIndex+1)+resultIndex
        self.__resultOfInterpolation = np.delete(self.__resultOfInterpolation, range, 0)

    def getApproximatedData(self):
        return self.__resultOfInterpolation
