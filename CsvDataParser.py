import csv
import sys
import numpy as np


class CsvDataParser:
    __csvFileDir = ""
    __elevationInfo = []
    __CSV_DISTANCE_INDEX = 0
    __CSV_ALT_INDEX = 0

    def __init__(self, distanceIndex, altIndex):
        self.__CSV_DISTANCE_INDEX = distanceIndex
        self.__CSV_ALT_INDEX = altIndex

    def retrieveCsvData(self):
        self.__loadCsvFileFromCLI()
        self.__putCsvDataIntoVariable()

    def __loadCsvFileFromCLI(self):
        try:
            self.__csvFileDir = sys.argv[1]
        except Exception as e:
            print("Wrong path name given!")

    def __putCsvDataIntoVariable(self):
        data = np.zeros(2)
        with open(self.__csvFileDir, newline='') as file:
            reader = csv.reader(file)
            for row in reader:
                data = np.vstack([data, (row[self.__CSV_DISTANCE_INDEX], row[self.__CSV_ALT_INDEX])])
        data = np.delete(data, 0, 0)
        data = np.delete(data, 0, 0)
        data = np.copy(data.astype(float))
        self.__elevationInfo = np.copy(data)
        file.close()

    def artificiallyConvertDistanceIntsToDoubles(self):
        indexOfInt = 1
        indexOfCurrent = 1
        while indexOfInt < len(self.__elevationInfo):
            doubleSpacing = 1.0
            intToConvert = self.__elevationInfo[indexOfInt][0]
            currentInt = self.__elevationInfo[indexOfCurrent][0]
            sameIntsCounter = 0
            while intToConvert == currentInt:
                sameIntsCounter += 1
                if indexOfCurrent + 1 >= len(self.__elevationInfo):
                    break;
                indexOfCurrent += 1
                currentInt = self.__elevationInfo[indexOfCurrent][0]
            doubleSpacing /= float(sameIntsCounter)
            constDoubleSpacing = doubleSpacing
            for i in range(indexOfInt, indexOfInt + sameIntsCounter):
                self.__elevationInfo[i][0] += doubleSpacing
                doubleSpacing += constDoubleSpacing
            indexOfInt += sameIntsCounter
            indexOfCurrent = indexOfInt

    def getElevationInfo(self):
        return self.__elevationInfo



