import random


class Generator:
    def fillDiag(self, matrix, value, shift=0):
        for i in range(0, matrix.rows):
            if i - shift < 0 or i - shift >= matrix.rows:
                pass
            else:
                matrix.matrix[i - shift][i] = value
        return

    def fillWithVal(self, matrix, value, rand=False, shift=0.0):
        random.seed()
        mul = 1.0
        for i in range(0, matrix.rows):
            for j in range(0, matrix.cols):
                if rand:
                    mul = random.random()
                matrix.matrix[i][j] = value * mul + shift

