import numpy as np
from errors.MatMulDimensionsError import MatMulDimensionsError


class Matrix:
    __L = None
    __P = None
    __U = None

    def __init__(self, rows, cols):
        self.__rows = rows
        self.__cols = cols
        self._matrix = np.zeros((rows, cols))

    def __init__(self, matrix):
        self._matrix = np.array(matrix)
        self.__cols = np.size(self.matrix, 1)
        self.__rows = np.size(self.matrix, 0)


    @property
    def rows(self):
        return self.__rows

    @property
    def cols(self):
        return self.__cols

    @property
    def matrix(self):
        return self._matrix

    @matrix.setter
    def matrix(self, mat):
        self._matrix = mat

    def transpose(self):
        fresh = np.zeros((self.__cols, self.__rows))  # reverse order
        for i in range(0, self.__rows):
            for j in range(0, self.__cols):
                fresh[j][i] = self.matrix[i][j]
        return Matrix(fresh)

    def at(self, i, j):  # access element of matrix
        try:
            return self.matrix[i][j]
        except IndexError:
            return None

    def multiply(self, m2):  # returns multiply self by m2
        if isinstance(m2, Matrix):
            if self.__cols == m2.rows:
                return Matrix(np.matmul(self.matrix, m2.matrix))
            else:
                raise MatMulDimensionsError
        else:  # m2 is single number
            result = Matrix(np.zeros((self.rows, self.cols)))
            for i in range(0, self.rows):
                for j in range(0, self.cols):
                    result.matrix[i][j] = self.matrix[i][j] * m2
            return result

    def add(self, m2):  # return added self and m2
        if self.cols == m2.cols and self.rows == m2.rows:
            result = np.zeros((self.rows, self.cols))
            for i in range(0, self.rows):
                for j in range(0, self.cols):
                    result[i][j] = self.matrix[i][j] + m2.matrix[i][j]
            return Matrix(result)
        else:
            raise MatMulDimensionsError



    def sub(self, m2):
        if self.cols == m2.cols and self.rows == m2.rows:
            result = np.zeros((self.rows, self.cols))
            for i in range(0, self.rows):
                for j in range(0, self.cols):
                    result[i][j] = self.matrix[i][j] - m2.matrix[i][j]
            return Matrix(result)
        else:
            raise MatMulDimensionsError

    def tril(self):  # returns lower part of matrix
        result = np.zeros((self.rows, self.cols))
        for i in range(1, self.rows):
            for j in range(0, i):
                result[i][j] = self.matrix[i][j]
        return Matrix(result)

    def triu(self):  # return upper part of matrix
        result = np.zeros((self.rows, self.cols))
        for j in range(1, self.cols):
            for i in range(0, j):
                result[i][j] = self.matrix[i][j]
        return Matrix(result)

    def diag(self):  # returns diagonal of matrix
        result = np.zeros((self.rows, self.cols))
        for i in range(0, self.rows):
            result[i][i] = self.matrix[i][i]
        return Matrix(result)

    def vecNorm(self):  # norm of a vector matrix
        return np.sqrt(self.transpose().multiply(self).matrix[0][0])

    def LUFactorization(self):  # returns L and U
        self.__U = np.copy(np.array(self.matrix))
        self.__L = np.identity(self.rows)
        self.__P = np.identity(self.rows)
        m = self.rows
        for k in range(0, m - 1):
            self.__pivote(k)
            for j in range(k+1, m):
                self.__L[j, k] = self.__U[j, k]/self.__U[k, k]
                self.__U[j, k:m] = self.__U[j, k:m] - (self.__L[j, k]*self.__U[k, k:m])
        return Matrix(self.__L), Matrix(self.__U), Matrix(self.__P)

    def __pivote(self, currentIteration):
        indexOfPivot = 0
        pivot = np.max(abs(self.__U[currentIteration:self.__rows, currentIteration]))
        for j in range(currentIteration, self.__rows):
            if abs(self.__U[j, currentIteration]) == pivot:
                indexOfPivot = j
                break
        self.__interchangeRows(currentIteration, indexOfPivot)

    def __interchangeRows(self, currentIteration, indexOfPivot):
        self.__U[[currentIteration, indexOfPivot], currentIteration:self.__rows] = \
            np.copy(self.__U[[indexOfPivot, currentIteration], currentIteration:self.__rows])
        self.__L[[currentIteration, indexOfPivot], 0:currentIteration] = \
            np.copy(self.__L[[indexOfPivot, currentIteration], 0:currentIteration])
        self.__P[[currentIteration, indexOfPivot], :] = np.copy(self.__P[[indexOfPivot, currentIteration], :])

    def getMatrix(self):
        return self._matrix






