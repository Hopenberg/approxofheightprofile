from matrix.Matrix import Matrix
import numpy as np


class Inversion:
    def invDiag(self,diag):  # works only for diagonal matrix, easy to inverse diagonal matrix
        result = np.zeros((diag.rows, diag.cols))
        self._test = diag.rows
        self._testM = diag
        for i in range(0, diag.rows):
            result[i][i] = 1 / diag.matrix[i][i]
        return Matrix(result)

    # with forward substitution
    def invLowerTriangle(self, lower, diag):
        lu = lower.add(diag)
        templu = np.matrix(lu.matrix)  # A = L + D
        result = np.matrix(np.zeros((lu.rows, lu.cols)))  # X = A^-1
        right = np.matrix(np.zeros((lu.rows, 1)))  # b

        for j in range(0, lu.rows):
            right[j, 0] = 1
            for i in range(j, lu.rows):
                if i == j:  # this if to prevent index out of bound
                    if templu[i, i] == 0:
                        pass
                    else:
                        result[i, j] = right[i, 0] / templu[i, i]
                else:
                    tempsum = templu[i, j:i] * result[j:i, j]
                    if templu[i, i] == 0:
                        pass
                    else:
                        result[i, j] = (right[i, 0] - tempsum) / templu[i, i]
            right[j, 0] = 0
            templu[0:lu.rows, j] = 0
        return Matrix(np.array(result))





